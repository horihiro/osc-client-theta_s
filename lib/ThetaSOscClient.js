var request = require('superagent');

var debug = require('debug')('superagent');
var qs = require('qs');
var utils = require('superagent/lib/node/utils');
var Response = require('superagent/lib/node/response');
var superagent_response_end_orginal = request.Request.prototype.end;
var superagent_response_end_modified = function(fn){
  function noop(){};
  function isJSON(mime) {
    return /[\/+]json\b/.test(mime);
  }
  function isRedirect(code) {
    return ~[301, 302, 303, 305, 307, 308].indexOf(code);
  }
  function isBinary(mime, binTypes) {
    var parts = mime.split('/');
    var type = parts[0];
    var subtype = parts[1];

    return binTypes.indexOf(type)>=0;
  }
  function isText(mime) {
    var parts = mime.split('/');
    var type = parts[0];
    var subtype = parts[1];

    return 'text' == type
      || 'x-www-form-urlencoded' == subtype;
  }

  var self = this;
  var data = this._data;
  var req = this.request();
  var buffer = this._buffer;
  var method = this.method;
  var timeout = this._timeout;
  debug('%s %s', this.method, this.url);

  // store callback
  this._callback = fn || noop;

  // querystring
  try {
    var querystring = qs.stringify(this.qs, { indices: false });
    querystring += ((querystring.length && this.qsRaw.length) ? '&' : '') + this.qsRaw.join('&');
    req.path += querystring.length
      ? (~req.path.indexOf('?') ? '&' : '?') + querystring
      : '';
  } catch (e) {
    return this.callback(e);
  }

  // timeout
  if (timeout && !this._timer) {
    debug('timeout %sms %s %s', timeout, this.method, this.url);
    this._timer = setTimeout(function(){
      var err = new Error('timeout of ' + timeout + 'ms exceeded');
      err.timeout = timeout;
      err.code = 'ECONNABORTED';
      self.abort();
      self.callback(err);
    }, timeout);
  }

  // body
  if ('HEAD' != method && !req._headerSent) {
    // serialize stuff
    if ('string' != typeof data) {
      var contentType = req.getHeader('Content-Type')
      // Parse out just the content type from the header (ignore the charset)
      if (contentType) contentType = contentType.split(';')[0]
      var serialize = request.serialize[contentType];
      if (!serialize && isJSON(contentType)) serialize = request.serialize['application/json'];
      if (serialize) data = serialize(data);
    }

    // content-length
    if (data && !req.getHeader('Content-Length')) {
      this.set('Content-Length', Buffer.isBuffer(data) ? data.length : Buffer.byteLength(data));
    }
  }

  // response
  req.on('response', function(res){
    debug('%s %s -> %s', self.method, self.url, res.statusCode);
    var max = self._maxRedirects;
    var mime = utils.type(res.headers['content-type'] || '') || 'text/plain';
    var len = res.headers['content-length'];
    var type = mime.split('/');
    var subtype = type[1];
    var type = type[0];
    var multipart = 'multipart' == type;
    var redirect = isRedirect(res.statusCode);
    var parser = self._parser;

    self.res = res;

    if ('HEAD' == self.method) {
      var response = new Response(self);
      self.response = response;
      response.redirects = self._redirectList;
      self.emit('response', response);
      self.callback(null, response);
      self.emit('end');
      return;
    }

    if (self.piped) {
      return;
    }

    // redirect
    if (redirect && self._redirects++ != max) {
      return self.redirect(res);
    }

    // zlib support
    if (/^(deflate|gzip)$/.test(res.headers['content-encoding'])) {
      utils.unzip(req, res);
    }

    // don't buffer multipart
    if (multipart) buffer = false;

    // TODO: make all parsers take callbacks
    if (!parser && multipart) {
      var form = new formidable.IncomingForm;

      form.parse(res, function(err, fields, files){
        if (err) return self.callback(err);
        var response = new Response(self);
        self.response = response;
        response.body = fields;
        response.files = files;
        response.redirects = self._redirectList;
        self.emit('end');
        self.callback(null, response);
      });
      return;
    }

    // check for images, one more special treatment
    if (!parser && isBinary(mime, ["image", "video"])) {
      request.parse.image(res, function(err, obj){
        if (err) return self.callback(err);
        var response = new Response(self);
        self.response = response;
        response.body = obj;
        response.redirects = self._redirectList;
        self.emit('end');
        self.callback(null, response);
      });
      return;
    }

    // by default only buffer text/*, json and messed up thing from hell
    if (null == buffer && isText(mime) || isJSON(mime)) buffer = true;

    // parser
    var parse = 'text' == type
      ? request.parse.text
      : request.parse[mime];

    // everyone wants their own white-labeled json
    if (!parse && isJSON(mime)) parse = request.parse['application/json'];

    // buffered response
    if (buffer) parse = parse || request.parse.text;

    // explicit parser
    if (parser) parse = parser;

    // parse
    if (parse) {
      try {
        parse(res, function(err, obj){
          if (err && !self._aborted) self.callback(err);
          res.body = obj;
        });
      } catch (err) {
        self.callback(err);
        return;
      }
    }

    // unbuffered
    if (!buffer) {
      debug('unbuffered %s %s', self.method, self.url);
      self.res = res;
      var response = new Response(self);
      self.response = response;
      response.redirects = self._redirectList;
      self.emit('response', response);
      self.callback(null, response);
      if (multipart) return // allow multipart to handle end event
      res.on('end', function(){
        debug('end %s %s', self.method, self.url);
        self.emit('end');
      })
      return;
    }

    // terminating events
    self.res = res;
    res.on('error', function(err){
      self.callback(err, null);
    });
    res.on('end', function(){
      debug('end %s %s', self.method, self.url);
      // TODO: unless buffering emit earlier to stream
      var response = new Response(self);
      self.response = response;
      response.redirects = self._redirectList;
      self.emit('response', response);
      self.callback(null, response);
      self.emit('end');
    });
  });

  this.emit('request', this);

  // if a FormData instance got created, then we send that as the request body
  var formData = this._formData;
  if (formData) {

    // set headers
    var headers = formData.getHeaders();
    for (var i in headers) {
      debug('setting FormData header: "%s: %s"', i, headers[i]);
      req.setHeader(i, headers[i]);
    }

    // attempt to get "Content-Length" header
    formData.getLength(function(err, length) {
      // TODO: Add chunked encoding when no length (if err)

      debug('got FormData Content-Length: %s', length);
      if ('number' == typeof length) {
        req.setHeader('Content-Length', length);
      }

      var getProgressMonitor = function () {
        var lengthComputable = true;
        var total = req.getHeader('Content-Length');
        var loaded = 0;

        var progress = new Stream.Transform();
        progress._transform = function (chunk, encoding, cb) {
          loaded += chunk.length;
          self.emit('progress', {
            lengthComputable: lengthComputable,
            loaded: loaded,
            total: total
          });
          cb(null, chunk);
        };
        return progress;
      };
      formData.pipe(getProgressMonitor()).pipe(req);
    });
  } else {
    req.end(data);
  }

  return this;
};

var OscClient = require('osc-client').OscClient;
var makeHttpRequest = require('osc-client/lib/makeHttpRequest');
var commandsExecute = require('osc-client/lib/commandsExecute');
var ThetaSOscClient = function () {
    this.serverAddress = "http://192.168.1.1:80";
};
ThetaSOscClient.prototype = new OscClient("192.168.1.1","80");

//camera._finishWlan
ThetaSOscClient.prototype.finishWlan = function(sessionId, statusCallback) {
    return commandsExecute.apply(this, ['camera._finishWlan', { sessionId: sessionId }, statusCallback]);
};

//camera._startCapture
ThetaSOscClient.prototype.startCapture = function(sessionId, statusCallback) {
	return commandsExecute.apply(this, ['camera._startCapture', { sessionId: sessionId }, statusCallback]);
};

//camera._stopCapture
ThetaSOscClient.prototype.stopCapture = function(sessionId, statusCallback) {
    return commandsExecute.apply(this, ['camera._stopCapture', { sessionId: sessionId }, statusCallback]);
};

//camera._listAll
ThetaSOscClient.prototype.listAll = function(parameters, statusCallback) {
    return commandsExecute.apply(this, ['camera._listAll', parameters, statusCallback]);
};

//camera._getVideo
ThetaSOscClient.prototype.getVideo = function(fileUri, type, statusCallback) {
    request.Request.prototype.end = superagent_response_end_modified;
    var prms = commandsExecute.apply(this, ['camera._getVideo', { fileUri: fileUri, type: type }, statusCallback]);
    prms.then(function(res){
        request.Request.prototype.end = superagent_response_end_orginal;
    });
    return prms;
};

//camera._getLivePreview
ThetaSOscClient.prototype.getLivePreview = function(sessionId, statusCallback) {
    return commandsExecute.apply(this, ['camera._getLivePreview', { sessionId: sessionId }, statusCallback]);
};

module.exports = ThetaSOscClient;
